# Welcome to the final Git exercise.
# We're in a Python file again, but it looks like
# we've been beaten to the punch! Please
# follow the instructions in Learn to get the repository
# into a state where you can fill in your own answers!

"""
!!!!!
  DO NOT MODIFY ANY OF THE CODE BELOW THIS LINE
!!!!!
"""

first_question = "What is your favorite color?"
first_question_answer = ""

second_question = "What is your favorite food?"
second_question_answer = ""

third_question = "Who is your favorite fictional character? ex. Mickey Mouse, Bugs Bunny"
third_question_answer = ""

fourth_question = "What is your favorite animal?"
fourth_question_answer = ""

fifth_question = "What is your favorite programming language? (Hint: You can always say Python!!)"
fifth_question_answer = ""
